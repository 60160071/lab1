// const math = require('./math');
// const add = math.add1;
// // console.log(math.add1(1,1));
// // console.log(add(1,1));

const assert = require('assert');
describe('file to be tested', ()=> {
    context('function to be tested', ()=> {
        it('should do something', ()=>{
            assert.equal(1, 1);
        });
        it('should do another thing', ()=>{
            // assert.equal([1,2,3], [1,2,3]); error 
            assert.deepEqual([1,2,3], [1,2,3]); // pass 
            // assert.equal({name: 'oat'}, {name: 'oat'}); error
            assert.deepEqual({name: 'oat'}, {name: 'oat'}); // pass
        });
    });
});

const math = require('./math');
describe('file math', ()=>{
    context('function add1', ()=>{
        it('should add(1,2)', ()=>{
            // assert.equal(math.add1(1,2), 2); error
            assert.equal(math.add1(1,2), 3); // pass
        });      
    });
    context('function add2', ()=>{
        it('should add(1,2)', ()=>{
            // assert.equal(math.add2(5,5), 5); error
            assert.equal(math.add2(5,5), 10); // pass
        });      
    });
});