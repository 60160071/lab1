const chai = require('chai');
const expect = chai.expect;

describe('Test chai', ()=>{
    context('1', ()=>{
        it('should compare thing by expect', ()=>{
            expect(1).to.equal(1);
        });
    })
    context('2', ()=>{
        it('should compare another thing by expect', ()=>{
            // expect(5<8).to.be.false; error
            expect(5>8).to.be.false; //pass
            // expect({name: 'oat'}).to.equal({name: 'oat'}); error
            expect({name: 'oat'}).to.deep.equal({name: 'oat'}); // pass
            expect({name: 'oat'}).to.have.property('name').to.equal('oat'); //pass
            expect({}).to.be.a('object');
            expect(1).to.be.a('number');
            expect('oat').to.be.a('string');
            expect('oat'.length).to.equal(3);
            expect('oat').to.lengthOf(3);
            expect([1,2,3]).to.lengthOf(3);
            expect(null).to.be.null;
            expect(undefined).to.not.exist;
            expect(1).to.exist;
        });
    })
});

const math = require('./math')
describe('Math module', ()=>{
    context('Function add1', ()=>{
        it('should to return integer', ()=>{
            expect(math.add1(0,0)).to.be.a('number');
        });
        it('when add(1,1) should to return 2', ()=>{
            expect(math.add1(1,1)).to.equal(2);
        });
    })
});